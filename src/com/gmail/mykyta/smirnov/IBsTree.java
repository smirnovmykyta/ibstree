package com.gmail.mykyta.smirnov;

public interface IBsTree {
    void clear();
    void add(Person val);
    int size();
    int getWidth();
    int getHeight();
    int nodes();
    int leaves();
    Person[] toArray();
    Person del(Person val);
    String toString();
}
