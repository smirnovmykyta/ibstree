package com.gmail.mykyta.smirnov;

public class BsTreeR implements IBsTree {

    private Node root;

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public void add(Person value) {

    }

    @Override
    public int size() {
        return nodes() + leaves();
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }

    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    @Override
    public Person del(Person val) {
        return null;
    }
}
